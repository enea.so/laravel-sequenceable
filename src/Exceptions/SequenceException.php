<?php
/**
 * Created by eneasdh-fs
 * Date: 09/01/17
 * Time: 10:43 PM
 */

namespace Enea\Sequenceable\Exceptions;

use \Exception;

class SequenceException extends Exception
{
    // ...
}